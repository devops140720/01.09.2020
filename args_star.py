
def get_sum(a, b):
    return a + b

c = get_sum(1, 3)
#c = get_sum(1) # crash!
print(c)

# arguments -- optional parameter + with star
# usually names *args but as long as you write * it could be anything you want
# for example: *myargs
def foo(x, *args):
    print(f'x = {x}')
    print(type(args))
    for item in args:
        print(f'in args = {item}')
l = [1,2,3]
foo(3)
foo(3, 'hello', 'I' ,'Love','foo') # *args --> 'hello', 'I' ,'Love','foo' --> ['hello', 'I' ,'Love','foo'] Tuple
foo(3, 3, 3, 4, 7, 10) # *args --> 3, 3, 4, 7, 10 --> [3, 3, 4, 7, 10] Tuple

def foo2(x, list1=[]):
    print(f'x = {x}')
    #print(type(args))
    for item in list1:
        print(f'in list1 = {item}')

foo2(3, ['hello', 'I' ,'Love','foo'])
foo2(3)

print('===============================')
def get_sum_numbers(*args): # --> func2(*args) --> func3(*args)
    sum = 0
    for n in args:
        sum = sum + n
    return sum

x = get_sum_numbers()
y = get_sum_numbers(1, 5, 6, 7, -6, 200, -1000)
print(x)
print(y)


def run_numbers(win_numbers):
    win_numbers[0] = -1
    win_numbers[-1] = 8.345345


important_list = [17, 20, 22, 43, 44, 45]
run_numbers(tuple(important_list))
print(important_list)



