# 1. create a function with *args which returns the items in a list
#    l1 = get_args_as_list(1,3,3,4,5,6)
#    l1 will be [1,3,3,4,5,6]
# 2. create a function with also *args - get_into_list:
#    get_args_as_dictionary(a,b,*args)
#    l2 = get_args_as_dictionary(1, 3, 4, 5, 6)
#    l2 will be { 'positional_arguments' : [1, 3] , '*args' : [4,5,6] }
# 3. create a function get_tuple_from_list(list1)
#    l3 = get_tuple_from_list([1,2,3,4])
#    l3 will be tuple (1,2,3,4)
# 4. create a function counter_tuple(*args) will return count of items
#    l4 = counter_tuple('a', 'b', 'a', 1, 3, 1)
#    l4 will be { 'a' : 2, 'b' : 1, 1 : 2, 3 : 1 }
# 5. create function that will get a parameter - function discovery(param1)
#    discovery([1,2,3]) -- function will print : 'list can be modified in any way'
#    discovery((1,2,3)) -- function will print : 'tuple cannot be modified in any way'
#    discovery({ 'a' : 2, 'b' : 1, 1 : 2, 3 : 1 }) -- function will print : 'dictionary can be modified in any way'
#    discovery({1,2,3}) -- function will print : 'set can be modified. but duplicated values will be ignored!