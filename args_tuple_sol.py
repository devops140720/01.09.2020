'''
def get_sum(a, b):
    return a + b

c = get_sum(1, 3)
#c = get_sum(1) # crash!
print(c)

# arguments -- optional parameter + with star
# usually names *args but as long as you write * it could be anything you want
# for example: *myargs
def foo(x, *args):
    print(f'x = {x}')
    print(type(args))
    for item in args:
        print(f'in args = {item}')
l = [1,2,3]
foo(3)
foo(3, 'hello', 'I' ,'Love','foo') # *args --> 'hello', 'I' ,'Love','foo' --> ['hello', 'I' ,'Love','foo'] Tuple
foo(3, 3, 3, 4, 7, 10) # *args --> 3, 3, 4, 7, 10 --> [3, 3, 4, 7, 10] Tuple

def foo2(x, list1=[]):
    print(f'x = {x}')
    #print(type(args))
    for item in list1:
        print(f'in list1 = {item}')

foo2(3, ['hello', 'I' ,'Love','foo'])
foo2(3)

print('===============================')
def get_sum_numbers(*args): # --> func2(*args) --> func3(*args)
    sum = 0
    for n in args:
        sum = sum + n
    return sum

x = get_sum_numbers()
y = get_sum_numbers(1, 5, 6, 7, -6, 200, -1000)
print(x)
print(y)


def run_numbers(win_numbers):
    win_numbers[0] = -1
    win_numbers[-1] = 8.345345


important_list = [17, 20, 22, 43, 44, 45]
run_numbers(tuple(important_list))
print(important_list)
print('===============================')

def get_args_as_dictionary(a,b,c, *args):
    # (1,'a', 'b', 1,2,3,4,5)
    # positional arguments: 1, 'a' , 'b' ==> a,b,c
    # *args : 1,2,3,4,5
    # {'positional arguments' : [1,'a','b'], '*args' : [1,2,3,4,5] }
    pass

res = get_args_as_dictionary(1,'a', 'b', 1,2,3,4,5)
print(res)

'''

# 1. create a function with *args which returns the items in a list
#    l1 = get_args_as_list(1,3,3,4,5,6)
#    l1 will be [1,3,3,4,5,6]

def get_args_as_list(*args):
    result = list(args)
    return result
l1 = get_args_as_list(1, 3, 3, 4, 5, 6)
print(l1)

# 2. create a function with also *args - get_into_list:
#    get_args_as_dictionary(a,b,*args)
#    l2 = get_args_as_dictionary(1, 3, 4, 5, 6)
#    l2 will be { 'positional_arguments' : [1, 3] , '*args' : [4,5,6] }
def get_args_as_dictionary(a,b,c, *args):
    # (1,'a', 'b', 1,2,3,4,5)
    # positional arguments: 1, 'a' , 'b' ==> a,b,c
    # *args : 1,2,3,4,5
    # {'positional arguments' : [1,'a','b'], '*args' : [1,2,3,4,5] }
    result = dict()
    result['positional arguments'] = [a,b,c]
    result['args'] = list(args)
    return result

res = get_args_as_dictionary(1,'a', 'b', 1,2,3,4,5)
print(res)

# 3. create a function get_tuple_from_list(list1)
#    l3 = get_tuple_from_list([1,2,3,4])
#    l3 will be tuple (1,2,3,4)
def get_tuple_from_list(l1):
    result = tuple(l1)
    return result

def doesKeyExist(d, k):
    '''
    checks if key exists in dictionary
    :param d: dictionary
    :param k: key
    :return: True if k exist in d
    '''
    # return k in d.keys() -- shorter
    if k in d.keys():
        return True
    else:
        return False

# 4. create a function counter_tuple(*args) will return count of items
#    l4 = counter_tuple('a', 'b', 'a', 1, 3, 1)
#    l4 will be { 'a' : 2, 'b' : 1, 1 : 2, 3 : 1 }
def counter_tuple(*args):
    map_arg_to_count = dict()
    for arg1 in args:
        if doesKeyExist(map_arg_to_count, arg1):
            # map_word_to_count[word] = map_word_to_count[word] + 1
            old_value = map_arg_to_count[arg1]
            new_value = old_value + 1
            map_arg_to_count[arg1] = new_value  # inc the current value by 1
        else:
            map_arg_to_count[arg1] = 1  # creates new entry in the dict with key=word and value=1

# 5. create function that will get a parameter - function discovery(param1)
#    discovery([1,2,3]) -- function will print : 'list can be modified in any way'
#    discovery((1,2,3)) -- function will print : 'tuple cannot be modified in any way'
#    discovery({ 'a' : 2, 'b' : 1, 1 : 2, 3 : 1 }) -- function will print : 'dictionary can be modified in any way'
#    discovery({1,2,3}) -- function will print : 'set can be modified. but duplicated values will be ignored!'
def discovery(param1):
    if str(type(param1)) == "<class 'list'>":
        print('list can be modified in any way')
    # ...
discovery([1,2,3])
discovery({1,2,3})
discovery({1:'one',2:'two'})
discovery((1,2,3))

